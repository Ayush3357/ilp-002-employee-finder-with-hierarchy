﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinderWebService.Common
{
    public static class Util
    {
        /// <summary>
        /// Validates that <paramref name="param"/> is positive number.
        /// </summary>
        ///
        /// <param name="param">The parameter to validate.</param>
        /// <param name="paramName">The name of the parameter.</param>
        ///
        /// <exception cref="ArgumentException">If <paramref name="param"/> is not positive number.</exception>
        public static void ValidateArgumentPositive(long param, string paramName)
        {
            if (param <= 0)
            {
                throw new ArgumentException($"{paramName} should be positive.", paramName);
            }
        }
    }
}
