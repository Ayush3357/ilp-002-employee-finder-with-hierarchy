﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinderWebService.Common;
using FinderWebService.Model;
using FinderWebService.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FinderWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeRepository _employeeRepository;
        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        [HttpGet("{id}")]
        public EmployeeModel Get(string id)
        {
            EmployeeModel model = new EmployeeModel();
            model.Employee = _employeeRepository.Get(id);
            if(model.Employee != null)
            {
                if (model.Employee.ReportsTo != null && model.Employee.ReportsTo > 0)
                {
                    model.Manager = _employeeRepository.Get(model.Employee.ReportsTo.ToString());
                }
            }
            return model;
        }

        [HttpGet("{id}/all")]
        public IList<EmployeeModel> GetHeraracy(string id)
        {
            return GetEmployee(id);
        }

        private IList<EmployeeModel> GetEmployee(string id)
        {
            IList<EmployeeModel> list = new List<EmployeeModel>();
            EmployeeModel model = new EmployeeModel();
            model.Employee = _employeeRepository.Get(id);
            if (model.Employee != null)
            {
                if (model.Employee.ReportsTo != null && model.Employee.ReportsTo > 0)
                {
                    model.Manager = _employeeRepository.Get(model.Employee.ReportsTo.ToString());
                    list.Add(model);
                    while (model.Manager != null)
                    {
                        id = model.Manager.Id.ToString();
                        model = new EmployeeModel();
                        model.Employee = _employeeRepository.Get(id);
                        model.Manager = null;
                        if (model.Employee.ReportsTo != null && model.Employee.ReportsTo > 0)
                        {
                            model.Manager = _employeeRepository.Get(model.Employee.ReportsTo.ToString());
                        }
                        list.Add(model);
                    }
                }
            }
            return list;
        }
    }
}