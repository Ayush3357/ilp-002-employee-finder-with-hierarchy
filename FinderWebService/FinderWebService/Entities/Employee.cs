﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinderWebService.Entities
{
    public class Employee : IdentifiableEntity
    {
        [MaxLength(150)]
        [Required]
        public string Name { get; set; }

        [MaxLength(250)]
        [Required]
        public string Email { get; set; }

        public int? ReportsTo { get; set; }

        [MaxLength(2)]
        [Required]
        public string Band { get; set; }

        [MaxLength(10)]
        [Required]
        public string Phone { get; set; }

        [MaxLength(50)]
        [Required]
        public string WorkLocation { get; set; }
        
        [MaxLength(50)]
        [Required]
        public string Country { get; set; }
    }
}
