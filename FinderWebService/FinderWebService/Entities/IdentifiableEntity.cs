﻿using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FinderWebService.Entities
{
    public abstract class IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets a value indicating whether Id property should be serialized.
        /// </summary>
        /// <returns>True when not overridden.</returns>
        public virtual bool ShouldSerializeId()
        {
            return true;
        }
    }
}
