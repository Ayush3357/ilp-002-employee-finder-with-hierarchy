﻿using FinderWebService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinderWebService.Model
{
    public class EmployeeModel
    {
        public Employee Employee { get; set; }
        public Employee Manager { get; set; }
    }
}
