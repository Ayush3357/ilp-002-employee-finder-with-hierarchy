﻿using FinderWebService.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinderWebService.Repositories.Impl
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private AppDBContext _db;
        public EmployeeRepository(AppDBContext dBContext)
        {
            _db = dBContext;
        }
        public Employee Get(string id)
        {
            return _db.Employee.Where(x => x.Id.ToString() == id || x.Email.ToLower() == id.ToLower()).FirstOrDefault();
        }

        public IList<Employee> GetAll()
        {
            return _db.Employee.ToList();
        }

        public Employee GetByEmail(string emailId)
        {
            return _db.Employee.Where(x => x.Email == emailId).FirstOrDefault();
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}
