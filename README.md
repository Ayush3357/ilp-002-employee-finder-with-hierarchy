# ILP - 002 Employee Finder with Hierarchy

This Project contains .net core web services, sql data scripts and angular frontend for a complete employee finder solution. 

# Set up Database

```bash
Create_DB.sql
Create_Schema.sql
Insert_Data.sql
```

## Run Web Service

Update connection string to connect to Sql Server. Connection String is defined in appsettings.json and appsettings.development.json
Open FinderWebService.sln file in Visual Studio 2019
Execute project

## Run Front End

Install required packages 

```bash
npm install
```

Start Application

```bash
ng serve
```

Application will execute on http://localhost:4200/