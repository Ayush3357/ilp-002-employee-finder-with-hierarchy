Use MyWipro
Go
Drop table if exists dbo.Employee;
Create table dbo.Employee(
ID int identity(300000,1) not null primary key,
Name varchar(150) not null,
Email varchar(250) not null,
Band varchar(2) not null,
Phone varchar(10) not null,
ReportsTo int null,
WorkLocation varchar(50) not null,
Country varchar(50) not null
);
