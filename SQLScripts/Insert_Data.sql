Use MyWipro
Go

-- SET IDENTITY_INSERT to ON.  
SET IDENTITY_INSERT dbo.Employee ON;

insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300000, 'RISHAD PREMJI', 'RISHAD.PREMJI@WIPRO.COM','E', '8039916182', null, 'KARNATAKA', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300001, 'RAJAN KOHLI', 'RAJAN.KOHLI@WIPRO.COM','E', '6173208116', 300000, 'New Jersey', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300002, 'N S BALA', 'NSBALA@WIPRO.COM','E', '2145052741', 300000, 'California', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300003, 'BHANUMURTHY B M', 'BHANUMURTHY.BALLAPURAM@WIPRO.COM','E', '9980012153', 300000, 'KARNATAKA', 'India');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300004, 'SRINIVAS PALLIA', 'SRINIVAS.PALLIA@WIPRO.COM','E', '6123067012', 300000, 'New Jersey', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300005, 'ANAND PADMANABHAN', 'ANAND.PADMANABHAN@WIPRO.COM','E', '9845168390', 300000, 'KARNATAKA', 'India');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300006, 'ANGAN ARUN GUHA', 'ANGAN.GUHA@WIPRO.COM','E', '7324214748', 300000, 'New York', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300007, 'ANIL RAIBAGI MADHAV', 'ANIL.RAIBAGI@WIPRO.COM','E', '7322107541', 300000, 'New Jersey', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300008, 'ARJUN RAMARAJU', 'ARJUN.RAMARAJU@WIPRO.COM','D2', '9845106452', 300002, 'KARNATAKA', 'India');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300009, 'HARI MENON', 'HARI.MENON@WIPRO.COM','D2', '9845349120', 300002, 'KARNATAKA', 'India');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300010, 'HARIHARAN K', 'HARIHARAN.KRISHNA@WIPRO.COM','D2', '2142024356', 300002, 'Texas', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300011, 'RAJPAL GOHAR', 'RAJPAL.GOHAR@WIPRO.COM','D2', '4663648310', 300002, 'New South Wales', 'Australia');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300012, 'SAHADEV SINGH', 'SAHADEV.SINGH@WIPRO.COM','D2', '7787666889', 300002, 'Berkshire', 'United Kingdom');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300013, 'SARAT CHAND', 'SARAT.CHAND@WIPRO.COM','D2', '7595338851', 300002, 'London', 'United Kingdom');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300014, 'DHIRAJ WANCHOO', 'DHIRAJ.WANCHOO@WIPRO.COM','D2', '2142024356', 300002, 'Vienna', 'Austria');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300015, 'ASHWIN BHANDARKAR', 'ASHWIN.BHANDARKAR@WIPRO.COM','D2', '7506346924', 300002, 'MAHARASHTRA', 'India');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300016, 'ARISUDAN YADAV', 'ARISUDAN.YADAV@WIPRO.COM','D2', '7022984155', 300002, 'KARNATAKA', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300017, 'BHARATH SARMA REJETI', 'BHARATH.REJETI@WIPRO.COM','D2', '7259380675', 300002, 'KARNATAKA', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300018, 'ANINDITA CHATTOPADHYAY RAY', 'ANINDITA.RAY@WIPRO.COM','D2', '9038084153', 300002, 'WEST BENGAL', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300019, 'MAKARAND S. THIGALE', 'MAKARAND.THIGALE@WIPRO.COM','D1', '4037104397', 300018, 'Alberta', 'Canada');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300020, 'THIRUMALA PRASAD A', 'THIRUMALA.PRASAD@WIPRO.COM','D1', '8008299070', 300018, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300021, 'PRAVEEN MUPPIDI', 'PRAVEEN.MUPPIDI@WIPRO.COM','D1', '8008066643', 300018, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300022, 'HEMA VENKATA S R RALLAPALLI', 'HEMAVENKATA.RALLAPALLI@WIPRO.COM','D1', '9000169911', 300018, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300023, 'SUJATHA N RAO', 'SUJATHA.RAO@WIPRO.COM','D1', '9845096169', 300018, 'KARNATAKA', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300024, 'CHANDRA SEKHAR PEDAPATI', 'CHANDRA.PEDAPATI@WIPRO.COM','D1', '9704111986', 300018, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300025, 'FAREEDUDDIN MOHAMMAD', 'FAREEDUDDIN.MOHD@WIPRO.COM','D1', '7884116516', 300018, 'England', 'United Kingdom');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300026, 'ANUPAM DAS', 'ANUPAM.DAS2@WIPRO.COM','D1', '9007136052', 300018, 'MAHARASHTRA', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300027, 'ANUJ GOEL', 'ANUJGOEL@WIPRO.COM','D1', '7915667527', 300018, 'London', 'United Kingdom');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300028, 'PRAVEEN DANDU', 'PRAVEEN.DANDU@WIPRO.COM','C2', '9985138682', 300027, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300029, 'SURYA BHASKARARAO', 'SURYA.BHASKARARAO@WIPRO.COM','C2', '7548890125', 300027, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300030, 'B JAWAHAR', 'JAWAHAR.BALA@WIPRO.COM','C2', '7811846154', 300027, 'Leicestershire', 'United Kingdom');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300031, 'CHINMAYA NAYAK', 'CHINMAYA.NAYAK@WIPRO.COM','C2', '7915667527', 300027, 'Massachusetts', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300032, 'SIVARAM KANDA', 'SIVARAM.KANDA@WIPRO.COM','C1', '3159252804', 300031, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300033, 'SHATABDI PANDEY', 'SHATABDI.PANDEY@WIPRO.COM','C1', '9100947351', 300031, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300034, 'RAMESH GOLLA', 'RAMESH.GOLLA@WIPRO.COM','C1', '7702654297', 300031, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300035, 'GURURAJ KULKARNI MUSTOOR', 'GURURAJ.MUSTOOR@WIPRO.COM','C1', '9885152419', 300031, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300036, 'BOKAM HARIN CHANDRA MOULI', 'BOKAM.MOULI@WIPRO.COM','C1', '9652116058', 300031, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300037, 'KRISHNA SIDDAMSHETTY', 'KRISHNA.SIDDAMSHETTY1@WIPRO.COM','C1', '9866572235', 300031, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300038, 'VIJAY GUDAPATI', 'VIJAY.GUDAPATI@WIPRO.COM','B3', '4084808310', 300037, 'New York', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300039, 'KALLOL MONDAL', 'KALLOL.MONDAL@WIPRO.COM','B3', '2692719679', 300037, 'Massachusetts', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300040, 'MOHAMED IMRAN', 'MOHAMED.IMRAN@WIPRO.COM','B3', '3159280166', 300037, 'New York', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300041, 'DR. DEEPAK KUMAR AGARWAL', 'DEEPAK.AGARWAL1@WIPRO.COM','B2', '9900098084', 300037, 'KARNATAKA', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300042, 'AKHILESH KUMAR', 'AKHILESH.KUMAR1@APPIRIO.COM','B2', '6303011248', 300037, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300043, 'ELORITA MARTINEZ', 'ELORITA.MARTINEZ@WIPRO.COM','B2', '4013693507', 300037, 'Rhode Island', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300044, 'DONNA SATCHER ACKSON', 'DONNA.SATCHERJACKSON@WIPRO.COM','B2', '9867512345', 300037, 'New York', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300045, 'PARVEEZ RAICHUR', 'PARVEEZ.RAICHUR1@WIPRO.COM','B1', '7323228537', 300037, 'Massachusetts', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300046, 'SUDEEP DAS', 'SUDEEP.DAS3@WIPRO.COM','B1', '4793219458', 300037, 'Massachusetts', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300047, 'VASAVI LAHARI MUDDNA', 'VASAVI.MUDDNA@WIPRO.COM','B1', '9676390203', 300037, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300048, 'SNEHA', 'SNEHA.53@WIPRO.COM','B1', '8792484924', 300037, 'Telangana', 'INDIA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300049, 'MEDHA BHAT', 'MEDHA.BHAT@WIPRO.COM','B1', '9866572235', 300037, 'Massachusetts', 'USA');
insert into dbo.Employee(ID, Name, Email, Band, Phone, ReportsTo, WorkLocation, Country) values(300050, 'AYUSH SINGH TOMAR', 'AYUSH.TOMAR@WIPRO.COM','B1', '8075831497', 300037, 'Telangana', 'INDIA');

