export interface Employee{
    name : string;
    email : string;
    reportsTo: number;
    band: string;
    phone: number;
    workLocation: string;
    country: string;
    id: number
}