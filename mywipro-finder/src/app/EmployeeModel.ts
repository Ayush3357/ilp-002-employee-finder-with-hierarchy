import {Employee} from './Employee'

export interface EmployeeModel{
    employee: Employee;
    manager: Employee;
}