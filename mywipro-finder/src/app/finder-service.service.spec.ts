import { TestBed } from '@angular/core/testing';

import { FinderServiceService } from './finder-service.service';

describe('FinderServiceService', () => {
  let service: FinderServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FinderServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
