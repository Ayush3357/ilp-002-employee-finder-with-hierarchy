import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { EmployeeModel } from './EmployeeModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FinderServiceService {
  constructor(private http:HttpClient) {

  }

  getEmployees(id){
      return this.http.get(environment.api + "/api/employee/" + id + "/all");
  }

  getEmployeeDetails(id){
    return this.http.get(environment.api + "/api/employee/" + id);
  }
}
