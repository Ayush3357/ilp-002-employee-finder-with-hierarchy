import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinderMainComponent } from './finder-main.component';

describe('FinderMainComponent', () => {
  let component: FinderMainComponent;
  let fixture: ComponentFixture<FinderMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinderMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinderMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
