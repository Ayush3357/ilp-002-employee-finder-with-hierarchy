import { Component, OnInit, Injectable } from '@angular/core';
import { FinderServiceService } from 'src/app/finder-service.service';

@Component({
  selector: 'app-finder-main',
  templateUrl: './finder-main.component.html',
  styleUrls: ['./finder-main.component.css']
})

export class FinderMainComponent implements OnInit {
  employees:any;
  employeeDetails:any;
  constructor(private finderservice:FinderServiceService) { }
  ngOnInit(): void {
    this.employees = null;
  }

  submit(item){
    this.finderservice.getEmployees(item.value).subscribe(employees => {
        this.employees = employees.reverse();
    },
    error => alert(error.message)
    );
  }

  details(val){
    this.finderservice.getEmployeeDetails(val).subscribe(employee => {
      this.employeeDetails = employee;
    },
    error => alert(error.message)
    );
  }
}
