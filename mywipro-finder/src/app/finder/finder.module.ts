import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FinderMainComponent } from './finder-main/finder-main.component';



@NgModule({
  declarations: [FinderMainComponent],
  imports: [
    CommonModule
  ],
  exports:[
    FinderMainComponent
  ]
})
export class FinderModule { }
